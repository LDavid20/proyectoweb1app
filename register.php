<?php require('include/functions.php') ?>
<?php
// session_start();
// if ($_SESSION && $_SESSION['user']) {
// 	//user already logged in
// 	header('Location: /practica/proyecto/admin.php');
// }

$message = "";
if (!empty($_REQUEST['status'])) {
    switch ($_REQUEST['status']) {
        case 'success':
            $message = 'User was added succesfully';
          break;
          case 'error':
            $message = 'There was a problem inserting the user';
          break;
    }
}
?>
<?php require('include/head.php') ?>

<div class="limiter">
    <div class="container-login100">


        <!-- ajax contact form -->
        <section style="margin-top: 50px;">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">

                            <h5 class="card-header" style="background-color:#8c7ae6;color:white"> Registro de Usuario
                                <button type="button" class="close" aria-label="Close" onclick="location.href='index.php'">

                                    <span aria-hidden="true">&times;</span>

                                </button>
                            </h5>

                            <div class="card-body">
                                <form class="contact__form" method="post" action="include/createfriend.php">

                                    <!-- form message -->
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="alert alert-success contact__msg" style="display: none" role="alert">
                                                Tu mensaje fue enviado exitosamente.
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end message -->

                                    <!-- form element -->
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <input name="name" type="text" class="form-control" placeholder="Nombre" required>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <input name="lastname" type="text" class="form-control" placeholder="Apellido" required>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <input name="email" type="email" class="form-control" placeholder="E-mail" required>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <input name="phone" type="text" class="form-control" placeholder="Teléfono" required>
                                        </div>

                                        <div class="col-12 form-group">
                                            <input name="country" type="text" class="form-control" placeholder="Pais" required>
                                        </div>
                                        <div class="col-12 form-group">
                                            <textarea name="address" class="form-control" rows="3" placeholder="Dirección" required></textarea>
                                        </div>

                                        <div class="col-md-6 form-group">
                                            <input name="pass" type="password" class="form-control" placeholder="Contraseña" required>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <input name="repass" type="password" class="form-control" placeholder="Confirmación" required>
                                        </div>
                                        <div class="col-12">
                                            <input name="submit" type="submit" class="btn btn-success" value="Registrar">
                                            <div class="msg text-center">
                                                <?php echo $message; ?>
                                            </div>
                                        </div>
                                    </div>

                            </div>
                            <!-- end form element -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>


</div>
</div>

<?php require('include/footer.php') ?>