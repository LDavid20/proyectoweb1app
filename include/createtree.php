<?php
 require('tree.php');
   /**
 * Verifica los datos del arvol y los actualiza
 */

if (
  isset($_POST['name']) && isset($_POST['donate']) &&
  isset($_POST['idtree']) 
) {
    session_start();
    $user = $_SESSION['user'];
    $_POST['idfriend']= $user['id'];
    $_POST['heigth'] = 1;
    $_POST['date'] = date('Y-m-d');
    
    $ftree = new Tree();
  $saved = $ftree-> saveTree($_POST);

  if ($saved) {
    header('Location:  /practica/proyecto/mytree.php?status=success');
  } else {
    header('Location: /practica/proyecto/mytree.phpstatus=error');
  }
} else {
  header('Location: /practica/proyecto/mytree.php?status=error');
}
