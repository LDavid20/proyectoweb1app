
<?php require('include/friend.php') 

?>
<?php
session_start();
$user = $_SESSION['user'];
if (!$user) {
	header('Location: /practica/proyecto/index.php');
}

?>

<?php require('include/head.php') ?>

<div class="container">

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">TREE FRIENDS

		</a>
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
				<a class="nav-item nav-link" href="admin.php">Inicio <span class="sr-only">(current)</span></a>
				<?php if ($user['rol'] == 'administrador') { ?>
					<a class="nav-item nav-link active" href="adminfriends.php">Administracion Amigos</a>
          <a class="nav-item nav-link" href="alltree.php">Arboles</a>
				<?php } ?>
				<a class="nav-item nav-link" href="mytree.php">Mis Arboles</a>
			</div>

		</div>
		<a class="navbar" href="include/logout.php">Cerrar Seccion</a>
	</nav>
  <?php 
          $ffriend = new Friend();
          $friends = $ffriend -> getFriend();
          $count = 0;
          foreach ($friends as $type) {
              $count += 1;
          }
          $htmlCant = "<p class='text-center'> Cantidad de Amigos $count </p>";
          echo $htmlCant;
  ?>
  
    <table class="table table-light">
      <tbody>
        <tr>
          <td>Id</td>
          <td>Name</td>
          <td>Lastname</td>
          <td>Email</td>
          <td>Country</td>
          <td>Address</td>
          <td>Phone</td>
          <td>Actions</td>
        </tr>
        <?php
        $ffriend = new Friend();
          $friends = $ffriend -> getFriend();
          $friendsHtml = "";
          foreach ($friends as $friend) {
              if($friend['rol']==='administrador'){
                  $friendsHtml .= "<tr id='friend_{$friend['id']}'><td>{$friend['id']}</td><td>{$friend['name']}</td><td>{$friend['lastname']}</td><td>{$friend['email']}</td><td>{$friend['country']}</td><td>{$friend['address']}</td><td>{$friend['phone']}</td><td> <a href='editStudent.php?id={$friend['id']}'>Edit</a>| <a class='btn btn-primary disabled'>Delete</a></td></tr>";
              }
              else{
                  $friendsHtml .= "<tr id='friend_{$friend['id']}'><td>{$friend['id']}</td><td>{$friend['name']}</td><td>{$friend['lastname']}</td><td>{$friend['email']}</td><td>{$friend['country']}</td><td>{$friend['address']}</td><td>{$friend['phone']}</td><td> <a href='editStudent.php?id={$friend['id']}'>Edit</a>| <a href='deleteStudent.php?id={$friend['id']}' class='btn btn-primary' onclick='deleteStudent({$friend['id']})'>Delete</a></td></tr>";
              }

            // <a href='deleteStudent.php?id={$friend['id']}' class='btn btn-primary' onclick='deleteStudent({$friend['id']})'>Delete</a>
          }
          echo $friendsHtml;
        ?>
      </tbody>
    </table>

</div>


<?php require('include/footer.php') ?>