<?
// php require('include/functions.php') 
?>
<?php require('include/tree.php') ?>
<?php
session_start();
$user = $_SESSION['user'];
if (!$user) {
	header('Location: /practica/proyecto/index.php');
}
$message = "";
if (!empty($_REQUEST['status'])) {
    switch ($_REQUEST['status']) {
        case 'success':
            $message = 'User was added succesfully';
          break;
          case 'error':
            $message = 'There was a problem inserting the user';
          break;
    }
}
?>

<?php require('include/head.php') ?>

<div class="container">

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">TREE FRIENDS

		</a>
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
				<a class="nav-item nav-link" href="admin.php">Inicio <span class="sr-only">(current)</span></a>
				<?php if ($user['rol'] == 'administrador') { ?>
					<a class="nav-item nav-link" href="adminfriends.php">Administracion Amigos</a>
          <a class="nav-item nav-link" href="alltree.php">Arboles</a>
				<?php } ?>
				<a class="nav-item nav-link active" href="mytree.php">Mis Arboles</a>
			</div>

		</div>
		<a class="navbar" href="include/logout.php">Cerrar Seccion</a>
	</nav>
  <?php if ($user['rol'] == 'administrador') { ?>

				
    <form class="contact__form" method="post" action="include/createtree.php">
    <div class="page-header">
  <h1>Agrega tu árbol</h1>      
</div>
    <!-- <h3 class="panel-title">Título del panel con estilo h3</h3> -->
<!-- form message -->
<div class="row">
    <div class="col-12">
        <div class="alert alert-success contact__msg" style="display: none" role="alert">
            Tu mensaje fue enviado exitosamente.
        </div>
    </div>
</div>
<!-- end message -->

<!-- form element -->
<div class="row">
    <div class="col-md-6 form-group">
        <input name="name" type="text" class="form-control" placeholder="Nombre Del Árbol" required>
    </div>
    <div class="col-md-6 form-group">
        <input name="donate" type="number" class="form-control" placeholder="Donación" required min="1000" value="1000">
    </div>
    <div class="col-md-12 form-group">
    <!-- <div class="form-group"> -->
        <label class="sr-only" for="">Especie</label>
        <select class="mdb-select form-control" name="idtree">
        <option value="" disabled selected>Seleccione Especie</option>
        <?php
          $ftree = new Tree();     
          $trees = $ftree-> getTrees();
          $treesHtml = "";  
          foreach ($trees as $tree) {

            $treesHtml .= "<option name=\"{$tree['id']}\" value={$tree['id']}>{$tree['specie']}</option>";
          }
          echo $treesHtml;
        ?>
        </select>
      <!-- </div> -->
    </div>
    <div class="col-12 mb-3">
        <input name="submit" type="submit" class="btn btn-success" value="Donar">
        <div class="msg text-center">
            <?php echo $message; ?>
        </div>
    </div>
</div>

</div>
<!-- end form element -->
</form>
<?php } ?>
<div class="container">
<table class="table table-light ">
      <tbody>
        <thead class="thead-dark">
        <tr>
          <td  scope="col" >Id</td>
          <td  scope="col" >Especie</td>
          <td  scope="col" >Nombre</td>
          <td  scope="col" >Altura</td>
          <td  scope="col" >Fecha</td>
          <td  scope="col" >Acciones</td>
        </tr>
        </thead>
        <?php
          $ftree = new Tree();     
          $trees = $ftree-> getTreeFriends($user['id']);
        //   $trees = getFriend();
          $treesHtml = "";
          foreach ($trees as $tree) {
                  $treesHtml .= "<tr id='tree_{$tree['id']}'><td>{$tree['id']}</td><td>{$tree['specie']}</td><td>{$tree['name']}</td><td>{$tree['heigth']}</td><td>{$tree['date']}</td><td> <a href='viewtree.php?id={$tree['id']}' class='btn btn-primary' onclick='viewtree({$tree['id']})'>Ver</a></td></tr>";
          }
          echo $treesHtml;
        ?>
      </tbody>
    </table>
   </div>
</div>


<?php require('include/footer.php') ?>