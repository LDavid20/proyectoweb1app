<?
// php require('include/functions.php') 
?>
<?php require('include/tree.php') ?>
<?php
session_start();
$user = $_SESSION['user'];
$treeif="";
if (!$user) {
  header('Location: /practica/proyecto/index.php');
}
if($_GET){
  $pftree = new Tree();
  $_SESSION['idtreeinfo'] = $_GET['id'];
  $treeif = $pftree -> getTreeInfo($_GET['id']);
  $_SESSION['treeinfo'] =  $treeif;

}
$message = "";
if (!empty($_REQUEST['status'])) {

  switch ($_REQUEST['status']) {
    case 'success':
      $message = 'Image was added succesfully';
      break;
    case 'error':
      $message = 'There was a problem inserting the image';
      break;
  }
}

if ($_POST) {
  $pftree = new Tree();
  if ($filename = $pftree->uploadPicture('picture')) {

    $pfile['idtreeinfo'] =  $_SESSION['idtreeinfo'];
    $pfile['picturefile'] = $filename;
    $pfile['date'] = date('Y-m-d');

   
    $pictures = $pftree->savePicture($pfile);
  } else {
    echo "There was an error saving the picture";
  }
}

?>

<?php require('include/head.php') ?>

<div class="container">

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">TREE FRIENDS

    </a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-item nav-link" href="admin.php">Inicio <span class="sr-only">(current)</span></a>
        <?php if ($user['rol'] == 'administrador') { ?>
          <a class="nav-item nav-link" href="adminfriends.php">Administracion Amigos</a>
          <a class="nav-item nav-link" href="alltree.php">Arboles</a>
        <?php } ?>
        <a class="nav-item nav-link active" href="mytree.php">Mis Arboles</a>
      </div>

    </div>
    <a class="navbar" href="include/logout.php">Cerrar Seccion</a>
  </nav>
  <?php if ($user['rol'] == 'administrador') { ?>
					
				
  <form class="contact__form" method="POST" role="form" action="include/updatetree.php">
    <div class="page-header">
      <h1>Editar Imagen</h1>
    </div>

    <div class="row">

    <div class="col-md-6 form-group">
    <label for="name">Name</label>
        <input class="form-control" type="text" name="name" id="name" value="<?php echo $treeif['name'] ?>">
      </div>
      <div class="col-md-6 form-group">
        <label for="heigth">Heigth</label>
        <input class="form-control" type="number" min="0" name="heigth" id="heigth" value="<?php echo $treeif['heigth'] ?>">
      </div>
      <div class="col-md-12 form-group">
      <label class="sr-only" for="">Especie</label>
        <select class="mdb-select form-control" name="idtree" >">
        <option value=""  disabled selected>Seleccione Especie</option>
        <?php
          $ftree = new Tree();     
          $trees = $ftree-> getTrees();
          $treesHtml = "";  
          foreach ($trees as $tree) {
            if($treeif['idtree'] ==$tree['id']){
              $treesHtml .= "<option selected='true' name=\"{$tree['id']}\" value={$tree['id']}>{$tree['specie']}</option>";
            }else{
              $treesHtml .= "<option name=\"{$tree['id']}\" value={$tree['id']}>{$tree['specie']}</option>";
            }
          }
          echo $treesHtml;
        ?>
        </select>
      </div>
      <div class="col-12 mb-3">
        <!-- <img src="images/imgtree/2020.png" alt="" sizes="" srcset=""> -->
        <input name="submit" type="submit" class="btn btn-success" value="Guardar">
        <div class="msg text-center">
          <?php echo $message; ?>
        </div>
      </div>
    </div>

<!-- </div> -->
<!-- end form element -->
</form>
  <form class="contact__form" method="POST" role="form" action="viewtree.php"  enctype="multipart/form-data">
    <div class="page-header">
      <h1>Agregar Imagen</h1>
    </div>
    <!-- <h3 class="panel-title">Título del panel con estilo h3</h3> -->
    <!-- form message -->
    <div class="row">
      <div class="col-12">
        <div class="alert alert-success contact__msg" style="display: none" role="alert">
          Tu mensaje fue enviado exitosamente.
        </div>
      </div>
    </div>
    <!-- end message -->

    <!-- form element -->
    <div class="row">

      <div class="col-md-12 form-group">
        <input type="file" name="picture" id="picture">
      </div>
      <div class="col-12 mb-3">
        <!-- <img src="images/imgtree/2020.png" alt="" sizes="" srcset=""> -->
        <input name="submit" type="submit" class="btn btn-primary" value="Guardar">
        <div class="msg text-center">
          <?php echo $message; ?>
        </div>
      </div>
    </div>

</div>
<!-- end form element -->
</form>

<?php } ?>
<div class="container">
  <table class="table table-light ">
    <tbody>
      <thead class="thead-dark">
        <tr>
          <td scope="col">Id</td>
          <td scope="col">Fecha</td>
          <td scope="col">Foto</td>
          <td scope="col">Acción</td>
        </tr>
      </thead>
      <?php
      $ftree = new Tree();
      if ($_GET) {
        $pictures = $ftree->getImages($_GET['id']);
      } else {
        $pictures = $ftree->getImages($_SESSION['idtreeinfo']);
      }
      //   $trees = getFriend();
      $treesHtml = "";
      foreach ($pictures as $ptree) {
        if ($user['rol'] === "administrador") {
          $treesHtml .= "<tr id='tree_{$ptree['id']}'><td>{$ptree['id']}</td><td>{$ptree['date']}</td><td><img src=\"{$ptree['picturefile']}\" width='150px' height='150px'> </td><td> <a href='include/deletepicture.php?id={$ptree['id']}' class='btn btn-danger' onclick='include/deletepicture.php?id={$ptree['id']}'>Eliminar</a></td></tr>";
        } else {
          $treesHtml .= "<tr id='tree_{$ptree['id']}'><td>{$ptree['id']}</td><td>{$ptree['date']}</td><td><img src=\"{$ptree['picturefile']}\" width='150px' height='150px'> </td></tr>";
        }
      }
      echo $treesHtml;
      ?>
    </tbody>
  </table>
</div>
</div>


<?php require('include/footer.php') ?>