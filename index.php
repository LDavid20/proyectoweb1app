<?php require('include/functions.php') ?>
<?php
session_start();
if ($_SESSION && $_SESSION['user']) {
	//user already logged in
	header('Location: /practica/proyecto/admin.php');
}

$message = "";
if (!empty($_REQUEST['status'])) {
	switch ($_REQUEST['status']) {
		case 'login':
			$message = 'User does not exists';
			break;
		case 'error':
			$message = 'There was a problem inserting the user';
			break;
	}
}
?>
<?php require('include/head.php') ?>

<div class="limiter">
	<div class="container-login100">

		<div class="wrap-login100">

			<div class="login100-pic js-tilt" data-tilt>
				<img src="images/hojas.png" alt="IMG">
				<div class="msg text-center">
					<?php echo $message; ?>
				</div>
			</div>

			<form class="login100-form validate-form" method="POST" action="include/login.php" role="form">
				<span class="login100-form-title">
					Member Login
				</span>

				<div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
					<input class="input100" type="text" name="email" placeholder="Email">
					<span class="focus-input100"></span>
					<span class="symbol-input100">
						<i class="fa fa-envelope" aria-hidden="true"></i>
					</span>
				</div>

				<div class="wrap-input100 validate-input" data-validate="Password is required">
					<input class="input100" type="password" name="password" placeholder="Password">
					<span class="focus-input100"></span>
					<span class="symbol-input100">
						<i class="fa fa-lock" aria-hidden="true"></i>
					</span>
				</div>

				<div class="container-login100-form-btn">
					<button type="submit" class="login100-form-btn">
						Login
					</button>
				</div>

				<div class="text-center p-t-12">
					<span class="txt1">
						Forgot
					</span>
					<a class="txt2" href="#">
						Username / Password?
					</a>
				</div>

				<div class="text-center p-t-136">
					<a class="txt2" href="register.php">
						Create your Account
						<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
					</a>
				</div>
			</form>
		</div>
	</div>
</div>

<?php require('include/footer.php') ?>