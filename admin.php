<?
// php require('include/functions.php') 
?>
<?php require('include/friend.php') ?>
<?php
session_start();
// require 'funcs/conexion.php';
// include 'funcs/funcs.php';

// if(!isset($_SESSION["id_usuario"])){ //Si no ha iniciado sesión redirecciona a index.php
// 	header("Location: index.php");
// }
$user = $_SESSION['user'];
if (!$user) {
	header('Location: /practica/proyecto/index.php');
}

// $idUsuario = $_SESSION['id_usuario'];

// $sql = "SELECT id, nombre FROM usuarios WHERE id = '$idUsuario'";
// $result = $mysqli->query($sql);
$ffriend = new Friend();
$row = $ffriend-> getFriend();
?>


<!-- <link rel="stylesheet" href="css/bootstrap.min.css" >
		<link rel="stylesheet" href="css/bootstrap-theme.min.css" >
		<script src="js/bootstrap.min.js" ></script> -->

<?php require('include/head.php') ?>

<div class="container">

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">TREE FRIENDS

		</a>
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
				<a class="nav-item nav-link active" href="admin.php">Inicio <span class="sr-only">(current)</span></a>
				<?php if ($user['rol'] == 'administrador') { ?>
					<a class="nav-item nav-link" href="adminfriends.php">Administracion Amigos</a>
					<a class="nav-item nav-link" href="alltree.php">Arboles</a>
				<?php } ?>

				<a class="nav-item nav-link" href="mytree.php">Mis Arboles</a>
				<!-- <a class="nav-item nav-link disabled" href="#">Mis Arboles</a> -->
			</div>

		</div>
		<a class="navbar" href="include/logout.php">Cerrar Seccion</a>
	</nav>
	<div class="jumbotron">
		<h1> Bienvenido <?php echo $user['name'] ?> </h1>
		<br />
	</div>
	<blockquote>
	<?php 
          $ffriend = new Friend();
          $friends = $ffriend -> getFriend();
          $count = 0;
          foreach ($friends as $type) {
              $count += 1;
          }
          $htmlCant = "<p class='text-center'> Cantidad de Amigos $count </p>";
          echo $htmlCant;
  ?>

<?php 
          $fftree = new Friend();
          $ptrees = $fftree-> getTreesAll();
          $count = 0;
          foreach ($ptrees as $typea) {
              $count += 1;
          }
          $htmlCant = "<p class='text-center'> Cantidad de Arboles $count </p>";
          echo $htmlCant;
  ?>
		<p>Gracias por ayudar al ambiente</p>
	</blockquote>

</div>


<?php require('include/footer.php') ?>